//
//  ViewController.swift
//  CommunicationTest
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate  {
    
    var pokemonStatus: [String : Any]!
    
    // MARK: Outlets
    @IBOutlet weak var outputLabel: UITextView!
    
    // MARK: Required WCSessionDelegate variables
    // ------------------------------------------
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        //@TODO
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        //@TODO
    }
    
    // MARK: Receive messages from Watch
    // -----------------------------------
    
    // 3. This function is called when Phone receives message from Watch
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        // 1. When a message is received from the watch, output a message to the UI
        // NOTE: Since session() runs in background, you cannot directly update UI from the background thread.
        // Therefore, you need to wrap any UI updates inside a DispatchQueue for it to work properly.
        DispatchQueue.main.async {
            self.outputLabel.insertText("\nMessage Received: \(message)")
//            self.pokemonStatus = message
            
            let preferences = UserDefaults.standard
            preferences.set(message["Status"], forKey: "Status")
            preferences.set(message["Hunger"], forKey: "Hunger")
            preferences.set(message["Name"], forKey: "Name")
            preferences.set(message["Type"], forKey: "Type")
            // Checking the preference is saved or not
            self.didSave(preferences: preferences)
        }
        
        // 2. Also, print a debug message to the phone console
        // To make the debug message appear, see Moodle instructions
        print("Received a message from the watch: \(message)")
    }
    
    func getPokemonStatus() -> String{
        let preferences = UserDefaults.standard
        if preferences.string(forKey: "Status") != nil{
            let status = preferences.string(forKey: "Status")
            return status!
        } else {
            return ""
        }
    }
    
    func getPokemonHunger() -> String{
        let preferences = UserDefaults.standard
        if preferences.string(forKey: "Hunger") != nil{
            let hunger = preferences.string(forKey: "Hunger")
            return hunger!
        } else {
            return ""
        }
    }
    
    func getPokemonName() -> String{
        let preferences = UserDefaults.standard
        if preferences.string(forKey: "Name") != nil{
            let name = preferences.string(forKey: "Name")
            return name!
        } else {
            return ""
        }
    }
    
    func getPokemonType() -> String{
        let preferences = UserDefaults.standard
        if preferences.string(forKey: "Type") != nil{
            let type = preferences.string(forKey: "Type")
            return type!
        } else {
            return ""
        }
    }
    
    func didSave(preferences: UserDefaults){
        let didSave = preferences.synchronize()
        if !didSave{
            print("Preferences could not be saved!")
        }
    }

    
    // MARK: Default ViewController functions
    // -----------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // 1. Check if phone supports WCSessions
        print("view loaded")
        if WCSession.isSupported() {
            outputLabel.insertText("\nPhone supports WCSession")
            WCSession.default.delegate = self
            WCSession.default.activate()
            outputLabel.insertText("\nSession activated")
        }
        else {
            print("Phone does not support WCSession")
            outputLabel.insertText("\nPhone does not support WCSession")
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: Actions
    // -------------------
    @IBAction func sendMessageButtonPressed(_ sender: Any) {
        
        // 2. When person presses button, send message to watch
        outputLabel.insertText("\nTrying to send message to watch")
        
        // 1. Try to send a message to the phone
        if (WCSession.default.isReachable) {
            let message = ["Status" : "\(self.getPokemonStatus())",
            "Hunger" : "\(self.getPokemonHunger())",
            "Name": "\(self.getPokemonName())",
            "Type": "\(self.getPokemonType())"]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the UI
            outputLabel.insertText("\nMessage sent to watch")
            // output a debug message to the console
            print("Message sent to watch")
        }
        else {
            print("PHONE: Cannot reach watch")
            outputLabel.insertText("\nCannot reach watch")
        }
        
        
    }
    
    
    // MARK: Choose a Pokemon actions
    
    @IBAction func pokemonButtonPressed(_ sender: Any) {
        print("You pressed the pokemon button")
        pickCharacter(character: "pikachu")
        
    }
    
    @IBAction func caterpieButtonPressed(_ sender: Any) {
        print("You pressed the caterpie button")
        pickCharacter(character: "caterpie")
    }
    
    func pickCharacter(character: String){
        let character = ["character": character]
        if (WCSession.default.isReachable) {
            WCSession.default.sendMessage(character, replyHandler: nil)
            outputLabel.insertText("\nMessage sent to watch")
            print("Message sent to watch")
        } else {
            print("PHONE: Cannot reach watch")
            outputLabel.insertText("\nCannot reach watch")
        }
    }
    
    
}

