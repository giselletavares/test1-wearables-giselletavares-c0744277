//
//  Pokemon.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Giselle Tavares on 2019-10-31.
//  Copyright © 2019 Parrot. All rights reserved.
//

import Foundation

class Pokemon {
    var namePokemon: String = ""
    var imagePokemon: String = ""
    
    init(namePokemon: String, imagePokemon: String) {
        self.namePokemon = namePokemon
        self.imagePokemon = imagePokemon
    }
    
}
