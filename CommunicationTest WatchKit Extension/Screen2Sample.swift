//
//  Screen2Sample.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-31.
//  Copyright © 2019 Parrot. All rights reserved.
//

import Foundation
import WatchKit
import WatchConnectivity

@available(watchOSApplicationExtension 6.0, *)
@available(watchOSApplicationExtension 6.0, *)
@available(watchOSApplicationExtension 6.0, *)
class Screen2Sample: WKInterfaceController, WCSessionDelegate {
    
    var pokemon: Pokemon!
    var pokemonName: String! = ""
    var pokemonImage: String! = ""
    
    // MARK: Outlets
    // ---------------------

    // 1. Outlet for the image view
    @IBOutlet var pokemonImageView: WKInterfaceImage!
    
    // 2. Outlet for the label
    @IBOutlet var nameLabel: WKInterfaceLabel!
    
    // 3. Input for the pokemon name
    @IBOutlet var txtPokemonName: WKInterfaceTextField!
    
    @IBOutlet var lblAlert: WKInterfaceLabel!
    // MARK: Delegate functions
    // ---------------------
    
    // Default function, required by the WCSessionDelegate on the Watch side
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        pokemonImageView.setImageNamed("\(message["character"]!)")
        self.pokemonImage = message["character"]! as! String
    }
    
    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        
        // 1. Check if the watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        txtPokemonName.setHidden(true)
        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    // MARK: Actions
    @IBAction func startButtonPressed() {
        print("Start button pressed")
        
//        print("\(String(describing: self.pokemonName))")
//        print("\(String(describing: self.pokemonImage))")
//
//        if self.pokemonName == "" || self.pokemonImage == "" {
//            lblAlert.setText("Pick a Pokemon and a name")
//        } else {
//            print("hey!")
//            lblAlert.setText("HEYYY")
//            self.pokemon = Pokemon(namePokemon: self.pokemonName, imagePokemon: self.pokemonImage)
//            pushController(withName: "InterfaceVC", context: self.pokemon)
//        }
    
    }
    
    override func contextForSegue(withIdentifier segueIdentifier: String) -> Any? {
        if segueIdentifier == "InterfaceVC" {
            return Pokemon(namePokemon: self.pokemonName, imagePokemon: self.pokemonImage)
        }
        return nil
    }
    
    
    @IBAction func selectNameButtonPressed() {
        print("select name button pressed")
        txtPokemonName.setHidden(false)
    }
    
    @IBAction func getTxtValue(_ value: NSString?) {
        txtPokemonName.setHidden(true)
        self.pokemonName = value! as String
        nameLabel.setText("My name is \(value!)")
    }
    
    

}
