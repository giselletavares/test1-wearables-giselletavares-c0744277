//
//  InterfaceController.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate, WKExtensionDelegate {
    
    var myTimer : Timer?
    var isHibernate = false
    var elapsedTime : TimeInterval = 0.0 //time that has passed between pause/resume
    var startTime = NSDate()
    var duration : TimeInterval = 5.0 // 5 seconds
    
    var hp: Int = 100
    var hunger: Int = 0
    
    var pokemonName: String!
    var pokemonImage: String!

    // MARK: Outlets
    // ---------------------
    @IBOutlet var messageLabel: WKInterfaceLabel!
   
    // Imageview for the pokemon
    @IBOutlet var pokemonImageView: WKInterfaceImage!
    // Label for Pokemon name (Albert is hungry)
    @IBOutlet var nameLabel: WKInterfaceLabel!
    // Label for other messages (HP:100, Hunger:0)
    @IBOutlet var lblStatus: WKInterfaceLabel!
    @IBOutlet var outputLabel: WKInterfaceLabel!
    @IBOutlet var tmrPokemonTimer: WKInterfaceTimer!
    @IBOutlet var btnHibernate: WKInterfaceButton!
    
    
    
    // MARK: Delegate functions
    // ---------------------

    // Default function, required by the WCSessionDelegate on the Watch side
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
        
    }
    
    // 3. Get messages from PHONE - Wake Up
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("WATCH: Got message from Phone")
        // Message from phone comes in this format: ["course":"MADT"]
        messageLabel.setText("From phone - HP: \(String(describing: message["Status"]!)), etc.")
        self.hp = Int(message["Status"] as! String)!
        self.hunger = Int(message["Hunger"] as! String)!
        self.nameLabel.setText((message["Name"]! as! String))
        self.pokemonImageView.setImageNamed((message["Type"]! as! String))
        
        self.outputLabel.setText("HP: \(self.hp)  Hunger: \(self.hunger)")
        
        self.pokemonStatusLabel()
        
        self.isHibernate = false
        self.runningTime()
    }
    
    

    
    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        
        // 1. Check if teh watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        
        if let pokemon = context as? Pokemon {
            self.pokemonName = pokemon.namePokemon
            self.pokemonImage = pokemon.imagePokemon
            nameLabel.setText(pokemon.namePokemon)
            pokemonImageView.setImageNamed(pokemon.imagePokemon)
        }
        
        self.pokemonStatusLabel()
        
        self.outputLabel.setText("HP: \(self.hp)  Hunger: \(self.hunger)")
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        myTimer = Timer.scheduledTimer(timeInterval: duration, target: self, selector: #selector(timerDone), userInfo: nil, repeats: true)
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    // MARK: Actions
    // ---------------------
    
    // 2. When person presses button on watch, send a message to the phone
    @IBAction func buttonPressed() {
        
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            self.messageLabel.setText("Sending msg to watch")
            WCSession.default.sendMessage(
                ["name" : "Pritesh"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent, put something here if u are expecting a reply from the phone")
                    self.messageLabel.setText("Got reply from phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
                self.messageLabel.setText("Error sending message")
            })
        }
        else {
            print("Phone is not reachable")
            self.messageLabel.setText("Cannot reach phone")
        }
    }
    
    
    // MARK: Functions for Pokemon Parenting
    @IBAction func nameButtonPressed() {
        print("name button pressed")
    }

    @IBAction func startButtonPressed() {
        print("Start button pressed")
        self.isHibernate = false
        self.runningTime()
    }
    
    @IBAction func feedButtonPressed() {
        print("Feed button pressed")
        
        if self.hunger <= 100 && self.hunger >= 12 {
            self.hunger -= 12
        }
        
        self.outputLabel.setText("HP: \(self.hp)  Hunger: \(self.hunger)")
    }
    
    @IBAction func hibernateButtonPressed() {
        print("Hibernate button pressed")
        
        self.isHibernate = true
        self.runningTime()
        
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            self.messageLabel.setText("Sending msg to Phone")
            WCSession.default.sendMessage(
                ["Status" : "\(self.hp)",
                 "Hunger" : "\(self.hunger)",
                 "Name": "\(String(describing: self.pokemonName!))",
                 "Type": "\(String(describing: self.pokemonImage!))"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent, put something here if u are expecting a reply from the phone")
                    self.messageLabel.setText("Got reply from phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
                self.messageLabel.setText("Error sending message")
            })
        }
        else {
            print("Phone is not reachable")
            self.messageLabel.setText("Cannot reach phone")
        }
        
    }
    
    @objc func timerDone(){
        if self.hunger < 100 {
            self.hunger += 10
            
            if self.hunger >= 100 {
                self.hunger = 100
            }
        }
        
        if self.hp > 0 && self.hp <= 100 {
            if self.hunger >= 80 {
                self.hp -= 5
            }
        }
        
        self.outputLabel.setText("HP: \(self.hp)  Hunger: \(self.hunger)")
        
        self.pokemonStatusLabel()
    }
    
    func pokemonStatusLabel(){
        if self.hp > 0 && self.hunger < 80 {
            lblStatus.setText(" is healthy")
        }
        
        if self.hunger >= 80 {
            lblStatus.setText(" is hungry")
        }
        
        if self.hp <= 0 {
            lblStatus.setText(" is dead...")
            tmrPokemonTimer.stop()
        }
    }
    
    func runningTime(){
        if self.isHibernate {
            //stop the ticking of the internal timer
            myTimer!.invalidate()
        } else {
            myTimer = Timer.scheduledTimer(timeInterval: duration, target: self, selector: #selector(timerDone), userInfo: nil, repeats: true)
        }
    }

    
}
